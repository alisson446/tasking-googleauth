var express = require('express');
const fs = require('fs');
const https = require('https');
const url = require('url');
const { getAuthUrl, authorizeToken } = require('./authorizeToken');
const listFiles = require('./src/driveFiles');

var app = express();
app.use(express.static(__dirname + '/public'));

// Load client secrets from a local file.
fs.readFile('drive_client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Calendar API.
  // authorize(JSON.parse(content));

  const credentials = JSON.parse(content);

  app.get('/', function (req, res) {
	  res.send('<p>Please <a href="' + getAuthUrl(credentials) + '">sign in</a> with your google account.</p>');
	});

	app.get('/authorizeToken', function (req, res, next) {

	  var url_parts = url.parse(req.url, true);
	  var code = url_parts.query.code;

	  authorizeToken(credentials, code).then(auth => (
	  	listFiles(auth).then(() => {
	  		res.send();
	  	})
	  ));

	});

	app.get('/zapier', function (req, res) {
	  
	  const requestBody = JSON.stringify({
			id: '1',
			name: 'a'
		});

		const webHookUrl = 'https://hooks.zapier.com/hooks/catch/2328947/9v7dhm/';

	  const { host, path } = url.parse(webHookUrl);

	  const request = https.request({ 
			method: 'POST',
			host: host,
			path: path,
			headers: {
			  'Content-Type': 'application/json',
			  'Content-Length': Buffer.byteLength(requestBody)
			}
		}, response => res.send('zapier trigger'));

		request.write(requestBody);

		request.end();

	});

	app.listen(8000, function () {
	  console.log('App listening on port 8000!');
	});

});
