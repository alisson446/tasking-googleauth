const google = require('googleapis');

module.exports = function listFiles(auth) {
  const service = google.drive('v3');

  return new Promise((resolve, reject) => {
    service.files.list({
      auth: auth,
      pageSize: 10,
      fields: "nextPageToken, files(id, name)"
    }, function(err, response) {
      if (err) {
        console.log('The API returned an error: ' + err);
        return;
      }
      const files = response.files;
      if (files.length == 0) {
        console.log('No files found.');
      } else {
        console.log('Files:');
        for (var i = 0; i < files.length; i++) {
          const file = files[i];
          console.log('%s (%s)', file.name, file.id);

          if(i + 1 == files.length) {
            resolve();
          }
        }
      }
    });
  });
}
